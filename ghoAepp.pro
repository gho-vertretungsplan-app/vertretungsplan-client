TEMPLATE = app
TARGET = ghoAepp

QT += core gui qml quick quickcontrols2

CONFIG += c++17

android {
    include(3rdparty/kirigami/kirigami.pri)
    include(3rdparty/android_openssl/openssl.pri)
}

# Input
HEADERS += src/GHOApp.h \
           src/Cache.h \
           src/CoursesModel.h \
           src/EntryFilterModel.h \
           src/VertListModel.h \
           src/VertPlanItem.h \
           src/VertPlanModel.h \
           src/Auth.h \
           src/Globals.h \
           src/DataFetcher.h
SOURCES += src/GHOApp.cpp \
           src/Auth.cpp \
           src/Cache.cpp \
           src/CoursesModel.cpp \
           src/EntryFilterModel.cpp \
           src/main.cpp \
           src/VertPlanItem.cpp \
           src/VertPlanModel.cpp \
           src/DataFetcher.cpp \
           src/VertListModel.cpp
RESOURCES += src/qml.qrc kirigami-icons.qrc resources/resources.qrc

ANDROID_ABIS = armeabi-v7a

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
