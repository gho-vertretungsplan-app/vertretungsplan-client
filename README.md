# Vertretungsplan-App für die GHO

Dies ist die App zum personalisierten lesen des GHO-Vertretungsplans.

# Unterstützte Betriebsysteme

* Android
* Linux

Weitere sind relativ einfach hinzufügen, aber iOS benötigt einen bezahlten Apple Entwickler Account.

# Build instructions

Clone this repository recursively, to fetch submodules.
Then run `qmake` and `make` or open the .pro file with QtCreator.
