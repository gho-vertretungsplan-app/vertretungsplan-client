/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VERTLISTMODEL_H
#define VERTLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>

#include "DataFetcher.h"
#include "VertPlanItem.h"

class VertListModel : public QAbstractListModel
{
    Q_OBJECT

    // TODO: evaluate if necessary
    Q_PROPERTY(int rowCount READ rowCount NOTIFY rowCountChanged)

    Q_PROPERTY(QString title READ title NOTIFY titleChanged)

public:
    enum VertPlanRoles {
        LessonRole = Qt::UserRole + 1,
        RegularTeacherRole,
        SubstituteTeacherRole,
        SubstituteSubjectRole,
        SubstituteRoomRole,
        TypeRole,
        CommentRole,
        RegularSubjectRole,
        RegularRoomRole,
        CourseRole
    };
    Q_ENUM(VertPlanRoles)

    explicit VertListModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QHash<int,QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void handleNewData(const QJsonObject &object);

    int rowCount();

    QString title();

signals:
    void rowCountChanged();
    void titleChanged();

private:
    DataFetcher *m_fetcher;

    QVector<VertPlanItem> m_items;
    QString m_title;
};

#endif // VERTLISTMODEL_H
