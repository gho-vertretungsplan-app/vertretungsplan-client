/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017-2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick.Controls 2.12
import QtQuick.Layouts 1.0
import QtQuick 2.0

import org.kde.kirigami 2.2 as Kirigami

Kirigami.ScrollablePage {
    title: qsTr("Über")

    ColumnLayout {
        Kirigami.Icon {
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            height: Kirigami.Units.gridUnit * 10
            width: height
            source: "qrc:/images/logo.svg"
        }

        Kirigami.Heading {
            text: qsTr("Haftungsausschluss")
        }

        Label {
            Layout.fillHeight: true
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: qsTr("Für die Korrektheit der Vertretungsplandaten kann keine Gewährleistung übernommen werden, "
                       + "da die Daten aus dem offiziellen PDF entnommen werden. "
                       + "Leider ändert sich die Struktur des PDFs von Zeit zu Zeit, was es uns erschwert "
                       + "zuverlässige Daten daraus zu erhalten.")
        }

        Kirigami.Heading {
            text: qsTr("Datenschutz")
        }

        Label {
            Layout.fillHeight: true
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: qsTr("Die Nutzung der App erfordert die Übertragung deiner IP-Adresse an den Server. "
                        + "Außerdem wird der Benutzername und das Passwort zur Authentifizierung übertragen. "
                        + "Alle weiteren Eingaben in der App bleiben auf deinem Gerät gespeichert und werden nicht übertragen.")
        }
    }
}
