import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0

ColumnLayout {
    property string content
    property string name
    visible: content

    Label {
        font.bold: true
        text: name
        visible: content
    }
    Label {
        text: content
        visible: content
    }
}
