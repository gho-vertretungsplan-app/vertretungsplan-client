/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017-2019 Jonah Brüchert <jbb.mail@gmx.de>
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.2

import org.kde.kirigami 2.2 as Kirigami
import berlin.gho.vertretungsplan 1.0

Kirigami.Page {
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    topPadding: 0

    title: qsTr("Vertretungstabelle")

    BusyIndicator {
        anchors.centerIn: parent
        visible: DataFetcher.loading
    }

    ScrollView {
        anchors.fill: parent
        TableView {
            id: table
            model: VertPlanModel {}

            delegate: Rectangle {
                implicitWidth: label.implicitWidth + 40
                implicitHeight: label.implicitHeight + 5
                border.width: 0.5
                Label {
                    anchors.centerIn: parent
                    id: label
                    text: model.display
                }
            }
        }
    }
}
