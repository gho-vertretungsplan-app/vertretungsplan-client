/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017-2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.14
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.11 as Kirigami
import berlin.gho.vertretungsplan 1.0

Kirigami.ApplicationWindow {
    id: window
    visible: true
    width: 960
    height: 540

    property AuthPage authPage: null;

    Kirigami.PagePool {
        id: mainPagePool
    }

    globalDrawer: GlobalDrawer {
        id: drawer
    }

    controlsVisible: pageStack.layers.currentItem !== authPage
    pageStack.initialPage: mainPagePool.loadPage("ListPage.qml")

    Component.onCompleted: {
        if (!Auth.credentialsKnown) {
            if (!authPage) {
                authPage = pageStack.layers.push("qrc:/qml/AuthPage.qml", {"authReason": Auth.CredentialsNotKnown})
            }
        }
    }

    Connections {
        target: DataFetcher
        onUsedCache: showPassiveNotification(qsTr("Es konnten kein aktueller Vertretungsplan aus dem Internet geholt werden. "
                                                  + "Eine zwischengespeicherte Version wird stattdessen angezeigt."))
    }

    Connections {
        target: Auth
        onAuthNeeded: (reason) => {
            if (!authPage) {
                authPage = pageStack.layers.push("qrc:/qml/AuthPage.qml", {"authReason": reason})
            }
        }
    }
}
