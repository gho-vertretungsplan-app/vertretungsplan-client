import QtQuick 2.7
import org.kde.kirigami 2.5 as Kirigami

Rectangle {
    property alias text: label.text

    z: -2

    width: parent.width
    height: Kirigami.Units.gridUnit * 2
    color: Kirigami.Theme.backgroundColor
    Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.Window

    Kirigami.Heading {
        id: label
        level: 3
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: Kirigami.Units.smallSpacing * 2
    }
}
