/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017-2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick.Controls 2.0
import QtQuick 2.7

import org.kde.kirigami 2.11 as Kirigami

Kirigami.GlobalDrawer {
    bannerImageSource: "qrc:/images/banner.svg"
    actions: [
        Kirigami.PagePoolAction {
            text: qsTr("Vertretungsliste")
            icon.name: "view-list-details"
            pagePool: mainPagePool
            page: "ListPage.qml"
        },
        Kirigami.PagePoolAction {
            text: qsTr("Vertretungstabelle")
            icon.name: "table"
            pagePool: mainPagePool
            page: "TablePage.qml"
        },
        Kirigami.PagePoolAction {
            text: qsTr("Klassen")
            icon.name: "list-add"
            pagePool: mainPagePool
            page: "CoursesPage.qml"
        },
        Kirigami.PagePoolAction {
            text: qsTr("Über")
            icon.name: "help-about"
            pagePool: mainPagePool
            page: "AboutPage.qml"
        }
    ]
}
