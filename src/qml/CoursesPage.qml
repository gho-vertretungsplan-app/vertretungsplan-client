import QtQuick 2.0
import QtQuick.Controls 2.8
import QtQuick.Layouts 1.12

import org.kde.kirigami 2.12 as Kirigami
import berlin.gho.vertretungsplan 1.0

Kirigami.ScrollablePage {
    title: qsTr("Klassen")

    actions.main: Kirigami.Action {
        text: qsTr("Klasse hinzufügen")
        onTriggered: popup.open()
        icon.name: "list-add"
    }

    ListView {
        header: ColumnLayout {
            width: parent.width
            Kirigami.Heading {
                id: heading
                level: 3
                Layout.margins: Kirigami.Units.gridUnit
                Layout.fillHeight: true
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                text: qsTr("Füge alle Klassen hinzu, in / mit denen du Unterricht hast.")
            }
        }

        Kirigami.OverlaySheet {
            id: popup

            header: Kirigami.Heading {
                text: qsTr("Klasse hinzufügen")
            }

            onSheetOpenChanged: {
                if (sheetOpen) {
                    textField.forceActiveFocus()
                }
            }

            ColumnLayout {
                Layout.fillWidth: true
                Layout.preferredWidth: window.width

                Label {
                    text: qsTr("Klasse (z.B. 10.23 oder 12)")
                    Layout.fillWidth: true
                }

                TextField {
                    id: textField
                    Layout.fillWidth: true
                    placeholderText: qsTr("Klasse")
                }
                ToolButton {
                    Layout.alignment: Qt.AlignRight
                    enabled: textField.text || textField.preeditText
                    text: qsTr("Hinzufügen")
                    onClicked: {
                        if (textField.text) {
                            coursesModel.addCourse(textField.text)
                        } else {
                            coursesModel.addCourse(textField.preeditText)
                        }

                        textField.text = ""
                        popup.close()
                    }
                }
            }
        }

        model: CoursesModel {
            id: coursesModel
        }

        Kirigami.PlaceholderMessage {
            anchors.centerIn: parent
            visible: coursesModel.count === 0
            text: qsTr("Noch keine Klassen")

            helpfulAction: Kirigami.Action {
                text: qsTr("Klasse hinzufügen")
                onTriggered: popup.open()
            }
        }

        delegate: Kirigami.SwipeListItem {
            Kirigami.Heading {
                text: model.display
            }

            actions: [
                Kirigami.Action {
                    text: qsTr("Löschen")
                    icon.name: "delete"
                    onTriggered: coursesModel.removeCourse(index)
                }
            ]
        }
    }
}
