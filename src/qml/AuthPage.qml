import QtQuick.Controls 2.12
import QtQuick 2.7
import QtQuick.Layouts 1.0

import org.kde.kirigami 2.2 as Kirigami
import berlin.gho.vertretungsplan 1.0

Kirigami.Page {
    title: qsTr("Anmelden")

    onBackRequested: (event) => {
        event.accepted = true
        // Nothing else, we don't want to be able to go back from here.
    }

    property int authReason

    Column {
        anchors.fill: parent
        spacing: 3

        Kirigami.Heading {
            anchors.left: parent.left
            anchors.right: parent.right
            wrapMode: Text.WordWrap
            width: parent.width
            level: 3
            id: errorLabel
            text: {
                switch(authReason) {
                case Auth.CredentialsWrong:
                    return qsTr("Der Benutzername oder das Password ist falsch")
                case Auth.CredentialsNotKnown:
                    return qsTr("Bitte gib den Benutzernamen und das Passwort für den GHO-Vertretungsplan ein.")
                }
            }
        }

        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: 20
        }

        Label {
            text: qsTr("Benutzername")
        }
        TextField {
            id: userField
            width: parent.width
        }

        Label {
            text: qsTr("Passwort")
        }
        TextField {
            id: passwordField
            width: parent.width
            echoMode: TextInput.Password
        }

        Button {
            anchors.right: parent.right
            text: qsTr("Speichern")
            onClicked: {
                Auth.saveCredentials(userField.text, passwordField.text)
                pageStack.layers.pop()
            }
        }
    }
}
