import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick 2.0

import org.kde.kirigami 2.2 as Kirigami

Kirigami.ScrollablePage {
    title: modelData.display

    property QtObject modelData

    Column {
        spacing: 20

        DetailsItem {
            content: modelData.lesson
            name: "Stunde"
        }
        DetailsItem {
            content: modelData.regularTeacher
            name: "Lehrer"
        }
        DetailsItem {
            content: modelData.substituteTeacher
            name: "Vertretungslehrer"
        }
        DetailsItem {
            content: modelData.regularSubject
            name: "Fach"
        }
        DetailsItem {
            content: modelData.substituteSubject
            name: "Fach (Änderung)"
        }
        DetailsItem {
            content: modelData.regularRoom
            name: "Raum"
        }
        DetailsItem {
            content: modelData.substituteRoom
            name: "Raum (Änderung)"
        }
        DetailsItem {
            content: modelData.type
            name: "Art"
        }
        DetailsItem {
            content: modelData.comment
            name: "Kommentar"
        }
    }
}
