import QtQuick.Controls 2.5
import QtQuick 2.7
import QtQuick.Layouts 1.0

import org.kde.kirigami 2.12 as Kirigami
import berlin.gho.vertretungsplan 1.0

Kirigami.ScrollablePage {
    id: listPage
    title: DataFetcher.loading ? qsTr("Wird geladen…") : vertSourceModel.title

    property bool filter: true

    actions.main: Kirigami.Action {
        icon.name: "view-filter"
        text: filter ? qsTr("Alle Kurse") : qsTr("Meine Klasse")
        onTriggered: filter = !filter
    }

    ListView {
        id: list

        BusyIndicator {
            anchors.centerIn: parent
            visible: DataFetcher.loading && !listPage.refreshing
        }

        Kirigami.PlaceholderMessage {
            anchors.fill: parent
            visible: vertSourceModel.rowCount === 0 && !DataFetcher.loading
            text: "Es konnte kein Vertretungsplan gefunden werden"
        }

        Kirigami.PlaceholderMessage {
            anchors.fill: parent
            visible: vertSourceModel.rowCount !== 0 && list.count === 0 && coursesModel.count !== 0 && !DataFetcher.loading
            text: "Keine Stundenplanänderungen in deinen Kursen"
        }

        Kirigami.PlaceholderMessage {
            anchors.centerIn: parent
            icon.source: "qrc:/images/add_class.svg"

            text: qsTr("Füge zuerst deine Klasse hinzu")

            CoursesModel {
                id: coursesModel
            }

            visible: vertSourceModel.rowCount !== 0 && list.count === 0 && coursesModel.count === 0

            helpfulAction: Kirigami.PagePoolAction {
                text: qsTr("Zu den Klassen gehen")
                pagePool: mainPagePool
                checkable: false
                page: "CoursesPage.qml"
            }
        }

        model: EntryFilterModel {
            filterEnabled: filter
            sourceModel: VertListModel {
                id: vertSourceModel
            }
        }

        delegate: Kirigami.BasicListItem {
            text: model.display
            height: Kirigami.Units.gridUnit * 2

            onClicked: {
                pageStack.push("qrc:/qml/DetailsPage.qml", {"modelData": model})
            }
        }

        section.delegate: ListSectionHeader {
            text: section
        }
        section.property: "course"
    }

    supportsRefreshing: true
    onRefreshingChanged: {
        if (refreshing) {
            DataFetcher.refreshData();
        }
    }
    Connections {
        target: DataFetcher
        onNewData: refreshing = false
    }
}
