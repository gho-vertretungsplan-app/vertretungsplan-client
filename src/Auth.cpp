﻿/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Auth.h"

#include <QNetworkAccessManager>
#include <QAuthenticator>

#include "GHOApp.h"
#include "DataFetcher.h"
#include "Globals.h"

Auth::Auth(QObject *parent)
    : QObject(parent)
{
}

void Auth::saveCredentials(const QString &username, const QString &password)
{
    GHOApp::instance()->setUsername(username);
    GHOApp::instance()->setPassword(password);

    Q_EMIT credentialsKnownChanged();

    // Now that we can, fetch data
    GHOApp::instance()->fetcher()->refreshData();
}

bool Auth::credentialsKnown() const
{
    return (!GHOApp::instance()->username().isEmpty() && !GHOApp::instance()->password().isEmpty());
}

void Auth::handleAuthentication(QNetworkReply *reply, QAuthenticator *authenticator)
{
    if (m_authCount > 1) {
        resetAuthCount();
        Q_EMIT authNeeded(AuthReason::CredentialsWrong);
        reply->abort();
        return;
    }

    m_authCount++;

    if (!credentialsKnown()) {
        emit authNeeded(AuthReason::CredentialsNotKnown);
        reply->abort();
        return;
    }

    authenticator->setUser(GHOApp::instance()->username());
    authenticator->setPassword(GHOApp::instance()->password());
}

void Auth::resetAuthCount()
{
    m_authCount = 0;
}
