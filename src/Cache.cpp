/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Cache.h"

#include <QStandardPaths>
#include <QFile>
#include <QDebug>
#include <QDir>

QString Cache::jsonPath()
{
    const auto directory = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir(directory).mkpath(QStringLiteral("."));
    return directory + QStringLiteral("/vertretungsplan.json");
}

void Cache::saveJson(const QByteArray &json)
{

    QFile file(jsonPath());
    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Failed to open cache file for writing";
        return;
    }
    file.write(json);
}

QByteArray Cache::cachedJson()
{
    QFile file(jsonPath());
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Failed to open cache file for reading";
        return {};
    }

    return file.readAll();
}
