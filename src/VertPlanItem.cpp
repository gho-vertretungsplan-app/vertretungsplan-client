/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "VertPlanItem.h"

#include <QJsonObject>

/**
 * @brief VertPlanItem::fromJson Creates a new VertPlanItem with the data from JSON.
 *
 * @param obj The QJsonObject to parse the data from.
 *
 * @return The new VertPlanItem.
 */
VertPlanItem VertPlanItem::fromJson(const QJsonObject &obj)
{
    VertPlanItem item;
    item.setLesson(obj.value("lesson").toString());
    item.setRegularTeacher(obj.value("regularTeacher").toString());
    item.setSubstituteTeacher(obj.value("substituteTeacher").toString());
    item.setSubstituteSubject(obj.value("substituteSubject").toString());
    item.setSubstituteRoom(obj.value("substituteRoom").toString());
    item.setType(obj.value("type").toString());
    item.setComment(obj.value("comment").toString());
    item.setRegularSubject(obj.value("regularSubject").toString());
    item.setRegularRoom(obj.value("regularRoom").toString());
    item.setCourse(obj.value("course").toString());
    return item;
}

VertPlanItem::VertPlanItem() = default;

VertPlanItem::VertPlanItem(const VertPlanItem &other) = default;

VertPlanItem::~VertPlanItem() = default;

VertPlanItem &VertPlanItem::operator=(const VertPlanItem &other) = default;

/**
 * Returns the affected lesson(s) of the entry, e.g. "4" or "2/3" for the second and third lessons.
 */
QString VertPlanItem::lesson() const
{
    return m_lesson;
}

void VertPlanItem::setLesson(const QString &lesson)
{
    m_lesson = lesson;
}

/**
 * Returns the abbrieviation of the teacher of the regular class.
 */
QString VertPlanItem::regularTeacher() const
{
    return m_regularTeacher;
}

void VertPlanItem::setRegularTeacher(const QString &regularTeacher)
{
    m_regularTeacher = regularTeacher;
}

/**
 * Returns the abbrieviation of the new, substitute teacher.
 */
QString VertPlanItem::substituteTeacher() const
{
    return m_substituteTeacher;
}

void VertPlanItem::setSubstituteTeacher(const QString &substituteTeacher)
{
    m_substituteTeacher = substituteTeacher;
}

/**
 * Returns the subsitute subject.
 */
QString VertPlanItem::substituteSubject() const
{
    return m_substituteSubject;
}

void VertPlanItem::setSubstituteSubject(const QString &substituteSubject)
{
    m_substituteSubject = substituteSubject;
}

/**
 * Returns the new, substitute room.
 */
QString VertPlanItem::substituteRoom() const
{
    return m_substituteRoom;
}

void VertPlanItem::setSubstituteRoom(const QString &substituteRoom)
{
    m_substituteRoom = substituteRoom;
}

/**
 * Returns the type of the entry, e.g. "Entfall" or "Vertretung".
 */
QString VertPlanItem::type() const
{
    return m_type;
}

void VertPlanItem::setType(const QString &type)
{
    m_type = type;
}

/**
 * Returns the comment of the entry (empty in most cases).
 */
QString VertPlanItem::comment() const
{
    return m_comment;
}

void VertPlanItem::setComment(const QString &comment)
{
    m_comment = comment;
}

/**
 * Returns the regular subject or course number, e.g. "Sp" or "Ma E10".
 */
QString VertPlanItem::regularSubject() const
{
    return m_regularSubject;
}

void VertPlanItem::setRegularSubject(const QString &regularSubject)
{
    m_regularSubject = regularSubject;
}

/**
 * Returns the regular room number, e.g. "C21".
 */
QString VertPlanItem::regularRoom() const
{
    return m_regularRoom;
}

void VertPlanItem::setRegularRoom(const QString &regularRoom)
{
    m_regularRoom = regularRoom;
}

/**
 * Returns the course number, e.g. "10.23"
 */
QString VertPlanItem::course() const
{
    return m_course;
}

void VertPlanItem::setCourse(const QString &course)
{
    m_course = course;
}
