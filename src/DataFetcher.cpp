/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "DataFetcher.h"

#include <QUrl>
#include <QUrlQuery>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>

#include "Globals.h"
#include "GHOApp.h"
#include "Auth.h"
#include "Cache.h"

DataFetcher::DataFetcher(GHOApp *ghoApp, QObject *parent)
    : QObject(parent),
      m_manager(new QNetworkAccessManager(this))
{
    // when the manager finishes downloading, trigger `handleJsonData`
    connect(m_manager, &QNetworkAccessManager::finished,
            this, &DataFetcher::handleJsonData);
    // when the manager requires authentication, trigger `handleAuthenticationRequired`
    connect(m_manager, &QNetworkAccessManager::authenticationRequired,
            ghoApp->auth(), &Auth::handleAuthentication);
}

void DataFetcher::refreshData()
{
    setLoading(true);
    // manager will automatically trigger `handleJsonData`, when finished
    m_manager->get(QNetworkRequest(QUrl(JSON_DATA_HTTP_URL)));
}

void DataFetcher::handleJsonData(QNetworkReply *reply)
{
    setLoading(false);
    GHOApp::instance()->auth()->resetAuthCount();

    switch(reply->error()) {
    case QNetworkReply::NoError: {
        const QByteArray replyData = reply->readAll();
        Cache::saveJson(replyData);
        Q_EMIT newData(QJsonDocument::fromJson(replyData).object());
        break;
    }
    default:
        // Use cache
        if (const auto cachedJson = Cache::cachedJson(); !cachedJson.isEmpty()) {
            Q_EMIT usedCache();
            Q_EMIT newData(QJsonDocument::fromJson(cachedJson).object());
        }
        break;
    }
}

bool DataFetcher::loading() const
{
    return m_loading;
}

void DataFetcher::setLoading(bool loading)
{
    m_loading = loading;
    Q_EMIT loadingChanged();
}
