/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *  Copyright (C) 2017-2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QUrl>
#include <QQuickWindow>
#include <QSortFilterProxyModel>
#include <QQuickStyle>

#ifdef Q_OS_ANDROID
#include "../3rdparty/kirigami/src/kirigamiplugin.h"
#endif

#include "GHOApp.h"
#include "VertPlanModel.h"
#include "VertListModel.h"
#include "Auth.h"
#include "CoursesModel.h"
#include "Globals.h"
#include "EntryFilterModel.h"

int main(int argc, char *argv[])
{
    //
    // App
    //

    // attributes
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    // create a qt app
    QGuiApplication app(argc, argv);

    // name, display name, description
    QGuiApplication::setApplicationName(APPLICATION_NAME);
    QGuiApplication::setApplicationDisplayName(APPLICATION_DISPLAY_NAME);
    QGuiApplication::setApplicationVersion(VERSION_STRING);
    QGuiApplication::setOrganizationDomain(DEVELOPER_NAME);


    //
    // Back-end
    //

    auto ghoApp = std::make_unique<GHOApp>();

    qmlRegisterSingletonInstance<Auth>(APPLICATION_ID, 1, 0, "Auth", ghoApp->auth());
    qmlRegisterSingletonInstance<DataFetcher>(APPLICATION_ID, 1, 0, "DataFetcher", ghoApp->fetcher());

    qmlRegisterType<VertPlanModel>(APPLICATION_ID, 1, 0, "VertPlanModel");
    qmlRegisterType<VertListModel>(APPLICATION_ID, 1, 0, "VertListModel");
    qmlRegisterType<EntryFilterModel>(APPLICATION_ID, 1, 0, "EntryFilterModel");
    qmlRegisterAnonymousType<QAbstractItemModel>(APPLICATION_ID, 1);
    qmlRegisterType<CoursesModel>(APPLICATION_ID, 1, 0, "CoursesModel");

    //
    // QML-GUI
    //

    QQmlApplicationEngine engine;

#ifdef Q_OS_ANDROID
    KirigamiPlugin::registerTypes(&engine);
    KirigamiPlugin().registerTypes("org.kde.kirigami");
#endif

    QIcon::setThemeName("breeze");
    if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
        qDebug() << "QT_QUICK_CONTROLS_STYLE not set, setting to Material";
        qputenv("QT_QUICK_CONTROLS_STYLE", "Material");
        QQuickStyle::setStyle("Material");
    }
    engine.load(QUrl("qrc:/qml/main.qml"));

    // enter qt main loop
    return app.exec();
}
