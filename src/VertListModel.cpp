/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "VertListModel.h"

#include <QJsonArray>

#include "VertPlanItem.h"
#include "GHOApp.h"

VertListModel::VertListModel(QObject *parent) : QAbstractListModel(parent)
{
    m_fetcher = GHOApp::instance()->fetcher();
    connect(m_fetcher, &DataFetcher::newData, this, &VertListModel::handleNewData);
    connect(this, &QAbstractItemModel::modelReset, this, &VertListModel::rowCountChanged);

    m_fetcher->refreshData();
}

int VertListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_items.count();
}

QHash<int, QByteArray> VertListModel::roleNames() const
{
    return {
        {Qt::DisplayRole, "display"},
        {LessonRole, "lesson"},
        {RegularTeacherRole, "regularTeacher"},
        {SubstituteTeacherRole, "substituteTeacher"},
        {SubstituteSubjectRole, "substituteSubject"},
        {SubstituteRoomRole, "substituteRoom"},
        {TypeRole, "type"},
        {CommentRole, "comment"},
        {RegularSubjectRole, "regularSubject"},
        {RegularRoomRole, "regularRoom"},
        {CourseRole, "course"}
    };
}

QVariant VertListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_items.count())
        return {};

    switch(role) {
    case Qt::DisplayRole:
        return QStringLiteral("%1. Stunde %2 bei %3").arg(
                    m_items[index.row()].lesson(),
                    m_items[index.row()].regularSubject(),
                    m_items[index.row()].regularTeacher());
    case LessonRole:
        return m_items[index.row()].lesson();
    case RegularTeacherRole:
        return m_items[index.row()].regularTeacher();
    case SubstituteTeacherRole:
        return m_items[index.row()].substituteTeacher();
    case SubstituteSubjectRole:
        return m_items[index.row()].substituteSubject();
    case SubstituteRoomRole:
        return m_items[index.row()].substituteRoom();
    case TypeRole:
        return m_items[index.row()].type();
    case CommentRole:
        return m_items[index.row()].comment();
    case RegularSubjectRole:
        return m_items[index.row()].regularSubject();
    case RegularRoomRole:
        return m_items[index.row()].regularRoom();
    case CourseRole:
        return m_items[index.row()].course();
    }

    return {};
}

void VertListModel::handleNewData(const QJsonObject &object)
{
    beginResetModel();
    m_items.clear();
    for (const auto item : object.value("data").toArray()) {
        m_items.append(VertPlanItem::fromJson(item.toObject()));
    }
    endResetModel();
    m_title = object.value("title").toString();
    emit titleChanged();
}

int VertListModel::rowCount()
{
    return rowCount({});
}

QString VertListModel::title()
{
    return m_title;
}
