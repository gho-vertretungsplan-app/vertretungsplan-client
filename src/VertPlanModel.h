/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VERTPLANMODEL_H
#define VERTPLANMODEL_H

#include <QAbstractTableModel>
#include <QJsonArray>
#include <QMap>

#include "DataFetcher.h"

class QVariant;
class QByteArray;

class VertPlanModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    VertPlanModel(QObject *parent = nullptr);
    ~VertPlanModel() override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

public slots:
    void handleNewData(const QJsonObject &object);

private:
    QJsonArray m_data {};
    QVariantMap m_keys {};

    DataFetcher* m_fetcher;
};

#endif // VERTPLANMODEL_H
