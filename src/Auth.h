/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AUTHTESTER_H
#define AUTHTESTER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class Auth : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool credentialsKnown READ credentialsKnown NOTIFY credentialsKnownChanged)

public:
    enum AuthReason {
        CredentialsNotKnown,
        CredentialsWrong
    };
    Q_ENUM(AuthReason);

    explicit Auth(QObject *parent = nullptr);

    static Auth *instance();

    Q_INVOKABLE void saveCredentials(const QString &username, const QString &password);

    /**
     * @brief Returns whether credentials are already known and saved
     */
    bool credentialsKnown() const;

    /**
     * @brief Emit this when authentication is needed
     * @param Reason why (because the password is not known yet, or it is wrong)
     */
    Q_SIGNAL void authNeeded(Auth::AuthReason reason);

    /**
      * @brief Emitted when new credentials are saved
      */
    Q_SIGNAL void credentialsKnownChanged();

    void resetAuthCount();

public slots:
    /**
     * @brief Handles authentication with previously stored password
     */
    void handleAuthentication(QNetworkReply *reply,
                              QAuthenticator *authenticator);

private:
    int m_authCount = 0;
};

#endif // AUTHTESTER_H
