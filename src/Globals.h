/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOBALS_H
#define GLOBALS_H

constexpr auto JSON_DATA_HTTP_URL = "https://gho.kaidan.im/vertretungsplan.json";
constexpr auto APPLICATION_ID = "berlin.gho.vertretungsplan";
constexpr auto APPLICATION_NAME = "vertretungsplan";
constexpr auto APPLICATION_DISPLAY_NAME = "VertretungsplanApp";
constexpr auto VERSION_STRING = "0.1dev";
constexpr auto DEVELOPER_NAME = "jbb";

#endif // GLOBALS_H
