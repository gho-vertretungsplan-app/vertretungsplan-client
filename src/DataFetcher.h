﻿/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DATAFETCHER_H
#define DATAFETCHER_H

#include <QObject>
#include <QNetworkReply>
#include <QAuthenticator>
#include <QJsonObject>

class GHOApp;

class DataFetcher : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool loading READ loading WRITE setLoading NOTIFY loadingChanged)

public:
    explicit DataFetcher(GHOApp *ghoApp, QObject *parent = nullptr);

    Q_INVOKABLE void refreshData();

    /**
      * @brief emitted when the fetcher has got data, using the internet or the cache
      */
    Q_SIGNAL void newData(QJsonObject object);

    /**
      * @brief emitted when data from the cache was used instead of up-to-date one.
      */
    Q_SIGNAL void usedCache();

    bool loading() const;
    void setLoading(bool loading);
    Q_SIGNAL void loadingChanged();

private slots:
    void handleJsonData(QNetworkReply *reply);
private:
    QNetworkAccessManager *m_manager;
    bool m_loading;
};

#endif // DATAFETCHER_H
