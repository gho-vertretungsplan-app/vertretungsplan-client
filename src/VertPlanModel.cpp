/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "VertPlanModel.h"

#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>
#include <QFile>

#include "GHOApp.h"
#include "DataFetcher.h"


VertPlanModel::VertPlanModel(QObject *parent) : QAbstractTableModel(parent)
{
    m_fetcher = GHOApp::instance()->fetcher();
    m_fetcher->refreshData();

    m_keys.insert("course", "Kurs");
    m_keys.insert("lesson", "Stunde");
    m_keys.insert("regularRoom", "Raum");
    m_keys.insert("regularSubject", "Fach");
    m_keys.insert("regularTeacher", "Lehrer");
    m_keys.insert("substituteRoom", "Raum (Vertretung)");
    m_keys.insert("substituteSubject", "Fach (Vertretung)");
    m_keys.insert("substituteTeacher", "Lehrer (Vertretung)");
    m_keys.insert("type", "Typ");
    m_keys.insert("comment", "Kommentar");

    connect(m_fetcher, &DataFetcher::newData, this, &VertPlanModel::handleNewData);
}

VertPlanModel::~VertPlanModel() = default;

QVariant VertPlanModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_data.count())
        return {};

    const auto item = m_data[index.row()].toObject();
    switch(role) {
    case Qt::DisplayRole:
        return item.value(m_keys.keys()[index.column()]);
    }

    return {};
}

int VertPlanModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return m_data.count();
}

int VertPlanModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_data.first().toObject().count();
}

QVariant VertPlanModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation)
    Q_UNUSED(role)

    return m_keys.value(m_keys.keys().at(section));
}

void VertPlanModel::handleNewData(const QJsonObject &object)
{
    beginResetModel();
    m_data = object.value("data").toArray();

    m_data.prepend(QJsonObject::fromVariantMap(m_keys));
    endResetModel();
}
