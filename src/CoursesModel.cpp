/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CoursesModel.h"

#include "GHOApp.h"



CoursesModel::CoursesModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_courses(GHOApp::instance()->courses())
{
    connect(GHOApp::instance(), &GHOApp::coursesChanged, this, [this] {
        m_courses = GHOApp::instance()->courses();
        Q_EMIT coursesChanged();
    });
    connect(this, &CoursesModel::coursesChanged, this, &CoursesModel::countChanged);
}

int CoursesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_courses.count();
}

QVariant CoursesModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_courses.count())
        return {};

    switch(role) {
    case Qt::DisplayRole:
        return m_courses.at(index.row());
    }
    return {};
}

void CoursesModel::addCourse(const QString &name)
{
    beginInsertRows({}, m_courses.count(), m_courses.count());
    m_courses.append(name);
    endInsertRows();
    GHOApp::instance()->setCourses(m_courses);
}

void CoursesModel::removeCourse(const int row)
{
    beginRemoveRows({}, row, row);
    m_courses.removeAt(row);
    endRemoveRows();
    GHOApp::instance()->setCourses(m_courses);
}

QStringList CoursesModel::courses() const
{
    return m_courses;
}

int CoursesModel::count() const
{
    return m_courses.size();
}
