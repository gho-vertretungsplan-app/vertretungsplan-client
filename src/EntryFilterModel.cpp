/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "EntryFilterModel.h"

#include "VertListModel.h"
#include "GHOApp.h"

EntryFilterModel::EntryFilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
    , m_filterEnabled(true)
    , m_courses(GHOApp::instance()->courses())
{
    connect(GHOApp::instance(), &GHOApp::coursesChanged, this, [=] {
        beginResetModel();
        m_courses = GHOApp::instance()->courses();
        endResetModel();
    });
}

bool EntryFilterModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (m_filterEnabled) {
        const auto entryCourse = qvariant_cast<QString>(sourceModel()->data(sourceModel()->index(sourceRow, 0, sourceParent), VertListModel::CourseRole));

        if (entryCourse.isEmpty()) {
            return false;
        }

        return std::find_if(m_courses.cbegin(), m_courses.cend(), [&entryCourse](const QString &course) {
            return entryCourse.contains(course);
        }) != m_courses.cend();
    }

    return true;
}

bool EntryFilterModel::filterEnabled() const
{
    return m_filterEnabled;
}

void EntryFilterModel::setFilterEnabled(bool filterEnabled)
{
    if (m_filterEnabled != filterEnabled) {
        beginResetModel();
        m_filterEnabled = filterEnabled;
        Q_EMIT filterEnabledChanged();
        endResetModel();
    }
}
