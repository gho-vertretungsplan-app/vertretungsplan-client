﻿/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *  Copyright (C) 2017 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#include "GHOApp.h"
#include "VertPlanItem.h"
#include "VertPlanModel.h"

#include <QSettings>

#include "Globals.h"
#include "DataFetcher.h"
#include "Auth.h"

GHOApp *GHOApp::s_instance = nullptr;

GHOApp::GHOApp(QObject *parent)
    : QObject(parent)
    , m_settings(new QSettings(APPLICATION_NAME, APPLICATION_NAME, this))
    , m_auth(new Auth(this))
    , m_fetcher(new DataFetcher(this, this))
{
    Q_ASSERT(!GHOApp::s_instance);
    GHOApp::s_instance = this;
}

DataFetcher *GHOApp::fetcher() const
{
    return m_fetcher;
}

Auth *GHOApp::auth() const
{
    return m_auth;
}

QStringList GHOApp::courses() const
{
    return m_settings->value("filter/courses").toStringList();
}

void GHOApp::setCourses(const QStringList &courses)
{
    m_settings->setValue("filter/courses", courses);
    Q_EMIT coursesChanged();
}

QString GHOApp::username() const
{
    return m_settings->value("auth/username").toString();
}

void GHOApp::setUsername(const QString &username)
{
    m_settings->setValue("auth/username", username);
}

QString GHOApp::password() const
{
    return m_settings->value("auth/password").toString();
}

void GHOApp::setPassword(const QString &password)
{
    m_settings->setValue("auth/password", password);
    Q_EMIT passwordChanged();
}

GHOApp *GHOApp::instance()
{
    return GHOApp::s_instance;
}
