﻿/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *  Copyright (C) 2017 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GHOAPP_H
#define GHOAPP_H

#include <QObject>
#include <QQmlListProperty>
#include <QJsonObject>
#include <QSettings>

class QNetworkReply;
class QAuthenticator;
class VertPlanModel;
class DataFetcher;
class Auth;

class GHOApp : public QObject
{
    Q_OBJECT

public:
    GHOApp(QObject* parent = nullptr);

    static GHOApp *instance();

    DataFetcher *fetcher() const;
    Auth *auth() const;

    QStringList courses() const;
    void setCourses(const QStringList &courses);
    Q_SIGNAL void coursesChanged();

    QString username() const;
    void setUsername(const QString &username);
    Q_SIGNAL void usernameChanged();

    QString password() const;
    void setPassword(const QString &password);
    Q_SIGNAL void passwordChanged();

private:
    QSettings* m_settings;
    Auth *m_auth;
    DataFetcher *m_fetcher;

    static GHOApp *s_instance;
};

#endif // GHOAPP_H
