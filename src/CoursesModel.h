/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2020 Jonah Brüchert <jbb.mail@gmx.de>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COURSESMODEL_H
#define COURSESMODEL_H

#include <QObject>
#include <QAbstractListModel>

class CoursesModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QStringList courses READ courses NOTIFY coursesChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
    explicit CoursesModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void addCourse(const QString &name);
    Q_INVOKABLE void removeCourse(const int row);

    QStringList courses() const;

    int count() const;

signals:
    void coursesChanged();
    void countChanged();

private:
    QStringList m_courses;
};

#endif // COURSESMODEL_H
