/*
 *  GHO-Äpp - App to view the Vertretungsplan conviniently
 *
 *  Copyright (C) 2017 Linus Jahn <git@lnj.li>
 *
 *  GHO-Äpp is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GHO-Äpp is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GHO-Äpp. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VERTPLANITEM_H
#define VERTPLANITEM_H

#include <QString>

class QJsonObject;

/**
 * @class VertPlanItem represents an entry in the 'Vertretungsplan'.
 *
 * The item can be initialized from JSON using a @see QJsonObject.
 */
class VertPlanItem
{
public:
    static VertPlanItem fromJson(const QJsonObject &);

    VertPlanItem();
    VertPlanItem(const VertPlanItem &other);
    ~VertPlanItem();

    VertPlanItem &operator=(const VertPlanItem &other);

    QString lesson() const;
    void setLesson(const QString &lesson);

    QString regularTeacher() const;
    void setRegularTeacher(const QString &regularTeacher);

    QString substituteTeacher() const;
    void setSubstituteTeacher(const QString &substituteTeacher);

    QString substituteSubject() const;
    void setSubstituteSubject(const QString &substituteSubject);

    QString substituteRoom() const;
    void setSubstituteRoom(const QString &substituteRoom);

    QString type() const;
    void setType(const QString &type);

    QString comment() const;
    void setComment(const QString &comment);

    QString regularSubject() const;
    void setRegularSubject(const QString &regularSubject);

    QString regularRoom() const;
    void setRegularRoom(const QString &regularRoom);

    QString course() const;
    void setCourse(const QString &course);

private:
    QString m_lesson;
    QString m_regularTeacher;
    QString m_substituteTeacher;
    QString m_substituteSubject;
    QString m_substituteRoom;
    QString m_type;
    QString m_comment;
    QString m_regularSubject;
    QString m_regularRoom;
    QString m_course;
};

#endif // VERTPLANITEM_H
